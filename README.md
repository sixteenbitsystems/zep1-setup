![Sixteenbit Systems](images/sixteenbit-logo.png)

# Gameboy Zero Setup

This setup script is for builds using RetroPie, GPIO controller, and i2s for audio.

## What's in the box

1. Optimized [config.txt](settings/config.txt) which includes overclocking
1. Download Gameboy Zero splashscreen by [AJRedfern](https://www.sudomod.com/forum/viewtopic.php?f=8&t=1440)
1. Fix for splashscreen audio
1. Install the gbz35 theme and set it as default
1. Install the gbz35-dark
1. Enable 30sec autosave on roms
1. Disable 'wait for network' on boot
1. Install [retrogame](https://learn.adafruit.com/retro-gaming-with-raspberry-pi/adding-controls-software) and copy config
1. Install [i2s script](https://learn.adafruit.com/adafruit-max98357-i2s-class-d-mono-amp/raspberry-pi-usage)
1. Install [battery monitor](https://github.com/sixteenbit/Mintybatterymonitor)

---

## Running the installer

1. Download RetroPie 4.3 from the [official site](https://github.com/RetroPie/RetroPie-Setup/releases/download/4.3/retropie-4.3-rpi1_zero.img.gz)
1. Flash the .img to an SD card (e.g. using [Etcher](https://etcher.io/) or [Apple Pi Baker](https://www.tweaking4all.com/software/macosx-software/macosx-apple-pi-baker/))
1. Enable WIFI and SSH, by adding [wpa_supplicant.conf](settings/wpa_supplicant.conf) and a blank file called ssh
1. Connect to the GBZ through SSH
1. `git clone https://gitlab.com/sixteenbitsystems/zep1-setup.git`
1. `cd zep1-setup`
1. `sudo chmod +x Setup.sh`
1. `sudo ./Setup.sh YES`
1. Answer any questions during installing but type "N" for any questions asking you to reboot.
1. `sudo reboot now`
1. Post install you can run the i2s script to test that it's working with: `curl -sS https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/i2samp.sh | bash`