# GBZ Combo Script

This script is used in conjunction with ADS1015 board for battery monitoring and the use of a series of shortcut combos 
to perform various task.

This script will display a battery icon according to battery level and will show a warning video when reaching low level.  Upon critical battery level, the script will show a critical battery level warning video and then introduce a safe shutdown.  The battery monitoring can be toggled on or off with a combo shortcut.

Also included in this script is the ability to perform shortcut combos with the use of a mode button to change volume, dimming level, toggle wifi and bluetooth, and do a quick and graceful shutdown.  Also included is a combo to display the list of combos available.

The mode button is by default an independent button installed to GPIO 7 but the script has the ability to use 'Start' as a mode as well if no independent mode button is present.  This option can be changed by altering the value in /boot/combo/pinfile.txt from 7 to 15.

## Combo Shortcuts

* Mode + A   = Toggle Battery
* Mode + Y   = Toggle Wifi with Icon
* Mode + B   = Toggle Bluetooth with Icon
* Mode + X   = Initiate Safe Shutdown
* Mode + Dpad Up   = Volume Up with Icon
* Mode + Dpad Down   = Volume Down with Icon
* Mode + Right Shoulder =  Display Shortcut Cheatsheet

![cheat sheet](icons/cheat.png)

## Automated Software Install

Go to raspberry command prompt or SSH.

Make sure you are in the home directory by typing ```cd ~ ``` and then type:

```bash
wget https://gitlab.com/sixteenbitsystems/gbz-combo-script/raw/master/InstallComboScript.sh
```

Then type:

```bash
sudo git clone https://gitlab.com/sixteenbitsystems/gbz-combo-script.git
```

Then type:

```bash
sudo chmod +x InstallComboScript.sh
```
And then type:

```bash
sudo ./InstallComboScript.sh
```

Finally reboot to have it all start on boot with:

```bash
sudo reboot
```

## Credit

Full credit goes to HoolyHoo for the initial [MintyComboScript](https://github.com/HoolyHoo/MintyComboScript). I just
 modified for Gameboy Zero.